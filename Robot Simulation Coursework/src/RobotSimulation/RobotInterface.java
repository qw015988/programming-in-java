package RobotSimulation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class RobotInterface {
	
	private Scanner s;						// scanner used for input from user
	private RobotArena myArena;				// arena in which Robots are shown
	
	public RobotInterface() {
		s = new Scanner(System.in);			// set up scanner for user input
	  	myArena = new RobotArena(20, 6);	// create arena of size 20*6
	    	
        char ch = ' ';
	    do {
	    	System.out.print("Enter (A)dd Robot, get (I)nformation, (D)isplay robots, (M)ove all robots, (R)run all robots, (N)ew arena, (S)ave arena, (L)oad arena, or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        			myArena.addRobot();	// add a new Robot to arena
        			doDisplay();
        			break;
	       		case 'I' :
        		case 'i' :
	       			System.out.print(myArena.toString());
	           		break;
                case 'd':
                case 'D':
                    doDisplay();
                    break;
                case 'n':
                case 'N':
                    createArena();
                    doDisplay();
                    break;
                case 'm':
                case 'M':
                    myArena.moveAllRobots();
                    doDisplay();
                    break;
                case 'r':
                case 'R':
                    runRobots();
                    break;
                case 's':
                case 'S':
                    saveArena();
                    break;
                case 'l':
                case 'L':
                    loadArenafromFile();
                    doDisplay();
                    break;
	       		case 'x' : 	ch = 'X';				// when X detected program ends
	       			break;
        	}
	   	} while (ch != 'X');						// test if end
        s.close();									// close scanner
	    }
	
    public void runRobots() { //this function is to make all the robots move ten times with a delay of 200ms
    	for (int moves = 0; moves <= 10; moves++) { //for 10 moves,
    		myArena.moveAllRobots(); //Call the already made moveAllRobots function
    		System.out.println("\n\n\n\n\n\n\n\n"); //Print a bunch of space inbetween arenas to make the console look cleaner when printing arenas.
    		doDisplay(); //Displaying the acutal arena with the doDisplay function
    		try {
				Thread.sleep(200); //each time delaying by 200ms
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    }
    
    public void createArena() { //function to create an arena of a user inputted size
    	int x = 20; //initialises default values for the arena size
    	int y = 6;
    	try {
    		System.out.println("specify width of arena (minimum 6):"); //user specifies width and height which is set to x and y
	    	x = Integer.parseInt(s.next());
	    	System.out.println("specify height of arena (minimum 1):");
	    	y = Integer.parseInt(s.next());
    	} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
    		System.out.println("invalid input"); //catches any numberformat error and print invalid input
    	}
    	myArena = new RobotArena(x, y); //sets robot arena dimentions
    }
	
    public void doDisplay() {
    	System.out.println("\n\n\n\n\n\n\n\n\n\n"); //makes space before printing for cleanliness purposes
        ConsoleCanvas canvas = new ConsoleCanvas(myArena.returnWidth(), myArena.returnHeight(), "31015988"); //calls the ConsoleCavas function
        //using getter functions for the width and height, also supplying my studnet ID
        myArena.showRobots(canvas); //Calls the showRobots function which flicks through every robot and displays it in the arena.
        System.out.println(canvas.toString()); //finally calls the tostring function which prints the arena.
    }
    
    
    public void saveArena() {
    	JFileChooser chooser = new JFileChooser(); //creating a jfilechooser instance
        FileFilter txtFilter = new FileNameExtensionFilter("*.txt","txt"); //filters filenames with txt
        chooser.addChoosableFileFilter(txtFilter); //adding the textfilter to the filechooser
        chooser.setFileFilter(txtFilter); //setting filefilter to be the default filter
        chooser.showSaveDialog(null); //shows the save button allowing the user to save as a .txt

        File file = chooser.getSelectedFile();
        if(file == null){
            return;
        } //checking if no file was selected
        if(!file.getAbsolutePath().endsWith(".txt")){
            file = new File(file.getAbsolutePath() + ".txt");
        } //if the selected file doesnt have .txt at the end then .txt gets added
        FileWriter fw; //creating a filewriter
		try {
			fw = new FileWriter(file, false);	
			fw.write(myArena.arenatostring()); //writers the arena as a condensed string form to the file
			fw.close(); //closes the file
		} catch (IOException e) {
			e.printStackTrace();
		} //catches an IOException error
    }
    
    public RobotArena loadArenafromFile() {
    	JFileChooser chooser = new JFileChooser(); //creates a new jfilechooser instance
        FileFilter txtFilter = new FileNameExtensionFilter("*.txt","txt"); //filters filenames with txt
        chooser.addChoosableFileFilter(txtFilter); //adding the textfilter to the filechooser
        chooser.setFileFilter(txtFilter); //setting filefilter to be the default filter
        chooser.showOpenDialog(null); //shows the save button allowing the user to save as a .txt
        
        File file = chooser.getSelectedFile(); //getting selected file from jfilechooser
        RobotArena arena; //initialising RobotArena instance
        try{
            arena = loadArena(file); //calls the loadArena function
        } catch (IOException e){
            System.out.println("Invalid File. Loading From Defaults"); //incase of error, prints invalid file and creates a new robot arena of default sizes.
            arena =  new RobotArena(20,6);
        }
        return arena;
    }

    public RobotArena loadArena(File file) throws IOException  {
    	FileReader fr = new FileReader(file); //creating a filereader
        int i; //initialising int i
        String str = ""; //initialising string str
        try {
			while((i = fr.read()) > 0){
			    str = str + (char)i; //adds individual characters from the text file to str one at a time till the end of the file is reached.
			}
			System.out.println("Robots and their locations:\n" + str); //prints the robots and their locations in the condensed form
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        //This bit is just setting the dimensions of the robot arena
        List<String> Arenaandrobotlist = Arrays.asList(str.split("\n"));
        //creates a list called Arenaandrobotlist splitting the long string at each \n character, as each robot has a new line in the text file.
        List<String> Arenadimentions = Arrays.asList(Arenaandrobotlist.get(0).split(","));
        // creates a list Arenadimentions by getting the first element of Arenaandrobotlist and splits it by the ",".
        myArena =  new RobotArena(Integer.parseInt(Arenadimentions.get(0)),Integer.parseInt(Arenadimentions.get(1)));
        //creates a new robot arena using the 0th and 1st integer in the Arenadimentions list (converting each one to an integer as they are both currently strings).
        
        for (int robotcount = 1; robotcount < Arenaandrobotlist.size(); robotcount++) { //for every robot in the Arenaandrobotlist starting at 1 because the 0th item in the list is the dimentions
            List<String> RobotCoordinates = Arrays.asList(Arenaandrobotlist.get(robotcount).split(","));
            //Creating a new list for a single robot's coordinates from each line in Arenaandrobotlist, seperated by the commas
            int x = Integer.parseInt(RobotCoordinates.get(0)); //the robots x coordinate is the first item in each list
            int y = Integer.parseInt(RobotCoordinates.get(1)); //the robots y coordinate is the second item in each list
            int directionIndex = Integer.parseInt(RobotCoordinates.get(2)); //the robots direction is the third item in each list
            Robot.Direction[] directions = Robot.Direction.values(); //getting all the values of the directions enumerator
            Robot.Direction direction = directions[directionIndex]; //setting the robot direction to the direction index obtained from the text file.
            Robot newRobot = new Robot(x, y, direction); //calling the robot function passing it all the relevant arguments
            myArena.addGivenRobot(newRobot); //calls the addGivenRobot function which adds the robot to the robot array.
        }
        
		return myArena; //returns the arena
    }
    
	public static void main(String[] args) {
		RobotInterface r = new RobotInterface();	// just call the interface
	}
}
