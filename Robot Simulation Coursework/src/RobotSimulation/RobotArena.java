package RobotSimulation;

import java.util.Random;

import java.util.ArrayList;

import RobotSimulation.Robot.Direction;;

public class RobotArena {
	private Random randomnumber; //initialises a random number variable
	private int width; //initialises integer width
    private int height; //initialises integer height
    private ArrayList<Robot> robots; //initialises an array of Robots called robots

    public RobotArena(int width, int height) { //declares constructor RobotArena
        this.width = width; //width parameter passed to the width constructor
        this.height = height; //height parameter passed to the height constructor
        this.robots = new ArrayList<>(); //height constructor assigned a new array
        this.randomnumber = new Random(); //creates new instance of random and assigns it to randomnumber
    }

    public void addRobot() {
        int X, Y; //declares integers X and Y
        X = randomnumber.nextInt(this.width) + 1;
        Y = randomnumber.nextInt(this.height) + 1;
        //creates new coordinates for robots and calls the getRobotAt function to check that
        //there does not already exist a robot in these newly made coordinates. (using the isHere function)
        if (robots.size() == (this.width*this.height)) {
        	System.out.println("Arena is full");
        	return;
        }
        
        if (getRobotAt(X, Y) == null) {
        	Direction randomDirection = Direction.randomDirection();
        	Robot newRobot = new Robot(X, Y, randomDirection); 
        	this.robots.add(newRobot);
        }
        else {
        	addRobot();
        }
        //if these coordinates are not already in use, then the robot is added to the array...
        //if they are, the function is re-called.
    }
    
    public void addGivenRobot(Robot robot) {
    	this.robots.add(robot);
    }
    //function used to add robots to the robot array with given coordinates and direction (from loadfromfile function)

    public String toString() {
    	String robotlist = "Arena "+this.width+" by "+this.height+" with Robots:\n";
    	//states the size of the arena
        for (Robot robot : this.robots) {
            robotlist = robotlist + robot.toString() + "\n";
        }
        //loops through every robot in the array and adds their position in text form to the string
        //using the toString method
        return robotlist.toString();
        //returns the entire string as one ready to be printed
        
    }
    
    public Robot getRobotAt(int x, int y) {
        for (Robot robot : this.robots) {
            if (robot.isHere(x, y)) {
                return robot;
            }
        }
        return null;
        //checks if all robots in the array are not in the new given coordinates, if so then returns the robot
    }
    
    public void showRobots(ConsoleCanvas c) {
        for (Robot robot : this.robots) {
            robot.displayRobot(c);
        }
    }
    //flips through every robot in the array and calls displayRobot to display the robot in the arena
    
    public int returnWidth() {
        return this.width;
    }
    //getter function for arena width

    public int returnHeight() {
        return this.height;
    }
    //getter function for arena height
    
    public void moveAllRobots() {
        for (Robot robot : this.robots) {
            robot.tryToMove(this);
        }
    }
    //function to move all the robots
    
    public String arenatostring() {
    	String arenastring = "";
    	arenastring = arenastring + this.width + "," + this.height + "\n";
    	for (Robot robot : this.robots) {
            arenastring = arenastring + robot.robotstring() + "\n";
        }
    	return arenastring.toString();
    }
    //this function is converting arena size and each robots location and direction to a compact string form that is easy to unpack at the other end.

    public static void main(String[] args) {
        RobotArena a = new RobotArena(20, 10);
        a.addRobot();
        a.addRobot();
        a.addRobot();
        //System.out.println(a.toString());
        System.out.println();
    }
}