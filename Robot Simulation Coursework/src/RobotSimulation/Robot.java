package RobotSimulation;

import java.util.Random;

public class Robot {
	
	private static int robotcount = 0; //initialises static int for number of robots
    private int x; //initialises x coordiante for robot
    private int y; //initialises y coordiante for robot
    private int id; //initialises
    private Direction direction; //initialises integer for ID of robots
    
    public enum Direction {
        north, east, south, west;
    	// 4 options for directions

        public static Direction randomDirection() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
        // method for getting a random direction
        public Direction nextDirection() {
        	int nextOrdinal = (this.ordinal() + 1) % 4;
            return values()[nextOrdinal];
        }
        // method for getting the next direction
        
    }

    public Robot(int x, int y, Direction direction) {
        this.x = x; //sets x position of new robot
        this.y = y; //sets y position of new robot
        this.id = robotcount; //sets id of new robot to new ID number
        this.direction = direction; //sets direction of robot to given direction
        robotcount++; //increments robotcount so next robot has new id
    }
    
    public boolean isHere(int sx, int sy) {
        return this.x == sx && this.y == sy;
    }
    //checks to see if the new robot will be in an occupied position.

    public String toString() {
        return "Robot "+this.id+" is at "+this.x+","+this.y+" facing "+this.direction;
    }
    //string to return robot id, its position and direction
    
    public String robotstring() {
    	return this.x+","+this.y+","+this.direction.ordinal();
    }
    //this function is purely for the saving feature. It converts a robots position and direction to a compact format like "5,2,1" (3rd number being direction)
    
    public void displayRobot(ConsoleCanvas c) {
        c.showIt(this.x, this.y, "R");
    }
    //sets particular coordinates to R to represent robot using the showIt function
    
    public boolean canMoveHere(int x, int y, RobotArena arena) {
        return x > 0 && x < arena.returnWidth() + 1 && y > 0 && y < arena.returnHeight() + 1 && arena.getRobotAt(x, y) == null;
    }
    //this canmovehere function checks if the robot can move to a specific location reguardless of its current location as it is currently irrelevant.
    //it checks if the location isnt a border and that there is no robot in this location.
    
    public void tryToMove(RobotArena arena) {
        int new_X = this.x; //sets variables new_X and new_Y to the current robot coordinates
        int new_Y = this.y;

        switch (this.direction) { //a switch case to test against all the directions
            case north: //if its north decrease Y by 1 (which effectively moves the robot up)
                new_Y--;
                break;
            case east: //if its east increase x by 1 which moves it right
            	new_X++;
                break;
            case south: //if its south increase Y by 1 (which effectively moves the robot down)
            	new_Y++;
                break;
            case west: //if its east decrease x by 1 which moves it right
            	new_X--;
                break;
        }

        if (canMoveHere(new_X, new_Y, arena)) { //only do this if you can actually move here which is determined by the canMoveHere function
            this.x = new_X; //sets the coordinates to the new positions
            this.y = new_Y;
        } else {
            this.direction = Direction.randomDirection(); //if the robot cannot move to this new location, the direction of the robot is randomised
        }
    }
    
    public static void main(String[] args) {
        Robot a = new Robot(5, 3, Direction.north);
        System.out.println(a.toString());

        //Robot b = new Robot(2, 4);
        //System.out.println(b.toString());
    }
}
