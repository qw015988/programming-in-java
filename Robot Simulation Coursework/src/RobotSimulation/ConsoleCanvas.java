package RobotSimulation;

public class ConsoleCanvas {
	
	private int width; // initialises width integer
	private int height; // initialises height integer
	private String studentID; // initialises my studentID string
	private String[][] arena; // initialises 2D array for arena
	
    public ConsoleCanvas (int width, int height, String studentID) {
        this.width = width + 2; //width of arena + 2 because 1 hashtag is required either side
        this.height = height + 2; //height of arena + 2 because 1 hashtag is required on top and bottom
        this.studentID = studentID;
        this.arena = new String[height+2][width+2];
        CreateArena(); //calls the create arena function which lays out all the hashtags and spaces.
    }
    
    private void CreateArena() {
        for (int i = 0; i < this.height; i++) { //for every layer
            for (int j = 0; j < this.width; j++) { //for every collumn in this layer
                if (i == 0 || i == this.height - 1 || j == 0 || j == this.width - 1) { //if this position is a border
                    this.arena[i][j] = "#"; //set this position to a #
                } else {
                    this.arena[i][j] = " "; //else set this position to a space
                }
            }
        }
        int startposition = (this.width - this.studentID.length()) / 2; //to display my ID, the correct amount of hashtags must be replaced and at the right position.
        //to display the ID in the middle of the top row, we must find out where to start inserting the ID. This is calculated as seen above.
        for (int i = 0; i < this.studentID.length(); i++) { //for every position through the ID...
           this.arena[0][startposition + i] = Character.toString(this.studentID.charAt(i)); //replace this position in the arena with the current number in my ID.
        }
    }
    
    public void showIt(int x, int y, String letter) {
        this.arena[y][x] = letter;
    }
    //actually sets the certain position given to the given letter (R).
    
    public String toString() { // actually creating the string to print involves flicking through every position and adding each character to a string.
        String result = ""; //creating a string called result
        for (int i = 0; i < height; i++) { // for every row
            for (int j = 0; j < width; j++) { //for every column in this row...
                result = result + this.arena[i][j]; //add whatever is there to the string
            }
            result = result + ('\n'); //at the end of this row add a \n which starts a new line
        }
        return result.toString(); //returns the string
    }
    
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (20, 6, "31015988");
		c.showIt(5,3,"D");
		System.out.println(c.toString());
	}
}